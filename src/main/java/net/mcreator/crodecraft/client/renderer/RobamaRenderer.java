
package net.mcreator.crodecraft.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.crodecraft.entity.RobamaEntity;
import net.mcreator.crodecraft.client.model.ModelRobama;

public class RobamaRenderer extends MobRenderer<RobamaEntity, ModelRobama<RobamaEntity>> {
	public RobamaRenderer(EntityRendererProvider.Context context) {
		super(context, new ModelRobama(context.bakeLayer(ModelRobama.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(RobamaEntity entity) {
		return new ResourceLocation("crodecraft:textures/robama.png");
	}
}
