
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.crodecraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;

import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Entity;

import net.mcreator.crodecraft.entity.RobamaEntity;
import net.mcreator.crodecraft.entity.PlankEntity;
import net.mcreator.crodecraft.entity.MichaelEntity;
import net.mcreator.crodecraft.entity.FairyBamaEntity;
import net.mcreator.crodecraft.entity.CreeplingEntity;
import net.mcreator.crodecraft.entity.BigChungusEntity;
import net.mcreator.crodecraft.CrodecraftMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class CrodecraftModEntities {
	public static final DeferredRegister<EntityType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.ENTITIES, CrodecraftMod.MODID);
	public static final RegistryObject<EntityType<CreeplingEntity>> CREEPLING = register("creepling",
			EntityType.Builder.<CreeplingEntity>of(CreeplingEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true)
					.setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(CreeplingEntity::new)

					.sized(0.4f, 0.3f));
	public static final RegistryObject<EntityType<FairyBamaEntity>> FAIRY_BAMA = register("fairy_bama",
			EntityType.Builder.<FairyBamaEntity>of(FairyBamaEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true)
					.setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(FairyBamaEntity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<PlankEntity>> PLANK = register("plank",
			EntityType.Builder.<PlankEntity>of(PlankEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(50)
					.setUpdateInterval(3).setCustomClientFactory(PlankEntity::new)

					.sized(0.4f, 0.3f));
	public static final RegistryObject<EntityType<BigChungusEntity>> BIG_CHUNGUS = register("big_chungus",
			EntityType.Builder.<BigChungusEntity>of(BigChungusEntity::new, MobCategory.CREATURE).setShouldReceiveVelocityUpdates(true)
					.setTrackingRange(64).setUpdateInterval(3).setCustomClientFactory(BigChungusEntity::new)

					.sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<RobamaEntity>> ROBAMA = register("robama",
			EntityType.Builder.<RobamaEntity>of(RobamaEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(64)
					.setUpdateInterval(3).setCustomClientFactory(RobamaEntity::new).fireImmune().sized(0.6f, 1.8f));
	public static final RegistryObject<EntityType<MichaelEntity>> MICHAEL = register("michael",
			EntityType.Builder.<MichaelEntity>of(MichaelEntity::new, MobCategory.MONSTER).setShouldReceiveVelocityUpdates(true).setTrackingRange(100)
					.setUpdateInterval(3).setCustomClientFactory(MichaelEntity::new)

					.sized(0.6f, 1.8f));

	private static <T extends Entity> RegistryObject<EntityType<T>> register(String registryname, EntityType.Builder<T> entityTypeBuilder) {
		return REGISTRY.register(registryname, () -> (EntityType<T>) entityTypeBuilder.build(registryname));
	}

	@SubscribeEvent
	public static void init(FMLCommonSetupEvent event) {
		event.enqueueWork(() -> {
			CreeplingEntity.init();
			FairyBamaEntity.init();
			PlankEntity.init();
			BigChungusEntity.init();
			RobamaEntity.init();
			MichaelEntity.init();
		});
	}

	@SubscribeEvent
	public static void registerAttributes(EntityAttributeCreationEvent event) {
		event.put(CREEPLING.get(), CreeplingEntity.createAttributes().build());
		event.put(FAIRY_BAMA.get(), FairyBamaEntity.createAttributes().build());
		event.put(PLANK.get(), PlankEntity.createAttributes().build());
		event.put(BIG_CHUNGUS.get(), BigChungusEntity.createAttributes().build());
		event.put(ROBAMA.get(), RobamaEntity.createAttributes().build());
		event.put(MICHAEL.get(), MichaelEntity.createAttributes().build());
	}
}
