
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.crodecraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.block.Block;

import net.mcreator.crodecraft.block.TheHivePortalBlock;
import net.mcreator.crodecraft.block.ObameggBlock;
import net.mcreator.crodecraft.block.ObamaBlock;
import net.mcreator.crodecraft.block.LeanBlock;
import net.mcreator.crodecraft.block.FroomBlock;
import net.mcreator.crodecraft.block.FairygrassBlock;
import net.mcreator.crodecraft.block.FairybamaForestsPortalBlock;
import net.mcreator.crodecraft.block.EpicOreBlock;
import net.mcreator.crodecraft.block.DenimBlock;
import net.mcreator.crodecraft.block.CreepBlock;
import net.mcreator.crodecraft.block.CocaPasteBlock;
import net.mcreator.crodecraft.block.CocaBlockBlock;
import net.mcreator.crodecraft.block.CocaBlock;
import net.mcreator.crodecraft.block.BeanstalkBlock;
import net.mcreator.crodecraft.block.BarryRealmPortalBlock;
import net.mcreator.crodecraft.block.BaRockBlock;
import net.mcreator.crodecraft.CrodecraftMod;

public class CrodecraftModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, CrodecraftMod.MODID);
	public static final RegistryObject<Block> EPIC_ORE = REGISTRY.register("epic_ore", () -> new EpicOreBlock());
	public static final RegistryObject<Block> THE_HIVE_PORTAL = REGISTRY.register("the_hive_portal", () -> new TheHivePortalBlock());
	public static final RegistryObject<Block> DENIM = REGISTRY.register("denim", () -> new DenimBlock());
	public static final RegistryObject<Block> OBAMA = REGISTRY.register("obama", () -> new ObamaBlock());
	public static final RegistryObject<Block> CREEP = REGISTRY.register("creep", () -> new CreepBlock());
	public static final RegistryObject<Block> LEAN = REGISTRY.register("lean", () -> new LeanBlock());
	public static final RegistryObject<Block> COCA = REGISTRY.register("coca", () -> new CocaBlock());
	public static final RegistryObject<Block> COCA_PASTE = REGISTRY.register("coca_paste", () -> new CocaPasteBlock());
	public static final RegistryObject<Block> COCA_BLOCK = REGISTRY.register("coca_block", () -> new CocaBlockBlock());
	public static final RegistryObject<Block> BA_ROCK = REGISTRY.register("ba_rock", () -> new BaRockBlock());
	public static final RegistryObject<Block> FAIRYGRASS = REGISTRY.register("fairygrass", () -> new FairygrassBlock());
	public static final RegistryObject<Block> BARRY_REALM_PORTAL = REGISTRY.register("barry_realm_portal", () -> new BarryRealmPortalBlock());
	public static final RegistryObject<Block> BEANSTALK = REGISTRY.register("beanstalk", () -> new BeanstalkBlock());
	public static final RegistryObject<Block> FAIRYBAMA_FORESTS_PORTAL = REGISTRY.register("fairybama_forests_portal",
			() -> new FairybamaForestsPortalBlock());
	public static final RegistryObject<Block> FROOM = REGISTRY.register("froom", () -> new FroomBlock());
	public static final RegistryObject<Block> OBAMEGG = REGISTRY.register("obamegg", () -> new ObameggBlock());

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientSideHandler {
		@SubscribeEvent
		public static void clientSetup(FMLClientSetupEvent event) {
			ObamaBlock.registerRenderLayer();
			CocaBlock.registerRenderLayer();
			FroomBlock.registerRenderLayer();
		}

		@SubscribeEvent
		public static void blockColorLoad(ColorHandlerEvent.Block event) {
			ObamaBlock.blockColorLoad(event);
		}

		@SubscribeEvent
		public static void itemColorLoad(ColorHandlerEvent.Item event) {
			ObamaBlock.itemColorLoad(event);
		}
	}
}
