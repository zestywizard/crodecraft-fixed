
package net.mcreator.crodecraft.fluid;

import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.fluids.FluidAttributes;

import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.resources.ResourceLocation;

import net.mcreator.crodecraft.init.CrodecraftModItems;
import net.mcreator.crodecraft.init.CrodecraftModFluids;
import net.mcreator.crodecraft.init.CrodecraftModBlocks;

public abstract class LeanFluid extends ForgeFlowingFluid {
	public static final ForgeFlowingFluid.Properties PROPERTIES = new ForgeFlowingFluid.Properties(CrodecraftModFluids.LEAN,
			CrodecraftModFluids.FLOWING_LEAN,
			FluidAttributes.builder(new ResourceLocation("crodecraft:blocks/lean_still"), new ResourceLocation("crodecraft:blocks/lean_flow"))
					.luminosity(1)

					.sound(ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("item.honey_bottle.drink"))))
			.explosionResistance(100f).canMultiply()

			.bucket(CrodecraftModItems.LEAN_BUCKET).block(() -> (LiquidBlock) CrodecraftModBlocks.LEAN.get());

	private LeanFluid() {
		super(PROPERTIES);
	}

	public static class Source extends LeanFluid {
		public Source() {
			super();
		}

		public int getAmount(FluidState state) {
			return 8;
		}

		public boolean isSource(FluidState state) {
			return true;
		}
	}

	public static class Flowing extends LeanFluid {
		public Flowing() {
			super();
		}

		protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> builder) {
			super.createFluidStateDefinition(builder);
			builder.add(LEVEL);
		}

		public int getAmount(FluidState state) {
			return state.getValue(LEVEL);
		}

		public boolean isSource(FluidState state) {
			return false;
		}
	}
}
