
package net.mcreator.crodecraft.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.crodecraft.entity.FairyBamaEntity;
import net.mcreator.crodecraft.client.model.Modelfairybama;

public class FairyBamaRenderer extends MobRenderer<FairyBamaEntity, Modelfairybama<FairyBamaEntity>> {
	public FairyBamaRenderer(EntityRendererProvider.Context context) {
		super(context, new Modelfairybama(context.bakeLayer(Modelfairybama.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(FairyBamaEntity entity) {
		return new ResourceLocation("crodecraft:textures/fairybama.png");
	}
}
