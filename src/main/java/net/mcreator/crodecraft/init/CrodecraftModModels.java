
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.crodecraft.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.mcreator.crodecraft.client.model.Modelfairybama;
import net.mcreator.crodecraft.client.model.Modelchungusmodel;
import net.mcreator.crodecraft.client.model.ModelRobama;
import net.mcreator.crodecraft.client.model.ModelPlank;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = {Dist.CLIENT})
public class CrodecraftModModels {
	@SubscribeEvent
	public static void registerLayerDefinitions(EntityRenderersEvent.RegisterLayerDefinitions event) {
		event.registerLayerDefinition(ModelPlank.LAYER_LOCATION, ModelPlank::createBodyLayer);
		event.registerLayerDefinition(Modelchungusmodel.LAYER_LOCATION, Modelchungusmodel::createBodyLayer);
		event.registerLayerDefinition(Modelfairybama.LAYER_LOCATION, Modelfairybama::createBodyLayer);
		event.registerLayerDefinition(ModelRobama.LAYER_LOCATION, ModelRobama::createBodyLayer);
	}
}
