
package net.mcreator.crodecraft.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.crodecraft.entity.BigChungusEntity;
import net.mcreator.crodecraft.client.model.Modelchungusmodel;

public class BigChungusRenderer extends MobRenderer<BigChungusEntity, Modelchungusmodel<BigChungusEntity>> {
	public BigChungusRenderer(EntityRendererProvider.Context context) {
		super(context, new Modelchungusmodel(context.bakeLayer(Modelchungusmodel.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(BigChungusEntity entity) {
		return new ResourceLocation("crodecraft:textures/chungus.png");
	}
}
