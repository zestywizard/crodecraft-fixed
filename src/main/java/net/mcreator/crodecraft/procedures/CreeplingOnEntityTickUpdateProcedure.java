package net.mcreator.crodecraft.procedures;

import net.minecraft.world.level.block.state.properties.Property;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.entity.Entity;
import net.minecraft.core.BlockPos;

import net.mcreator.crodecraft.init.CrodecraftModBlocks;

import java.util.Map;

public class CreeplingOnEntityTickUpdateProcedure {
	public static void execute(LevelAccessor world, Entity entity) {
		if (entity == null)
			return;
		double targetHeight = 0;
		targetHeight = entity.getY() - 1;
		if (!((world.getBlockState(new BlockPos(entity.getX(), targetHeight, entity.getZ()))).getBlock() == Blocks.AIR)) {
			if (!((world.getBlockState(new BlockPos(entity.getX(), targetHeight, entity.getZ()))).getBlock() == Blocks.BEDROCK)) {
				if (!((world.getBlockState(new BlockPos(entity.getX(), targetHeight, entity.getZ()))).getBlock() == Blocks.WATER
						&& (world.getBlockState(new BlockPos(entity.getX(), targetHeight, entity.getZ()))).getBlock() == Blocks.WATER)) {
					if (!((world.getBlockState(new BlockPos(entity.getX(), targetHeight, entity.getZ()))).getBlock() == Blocks.END_PORTAL_FRAME)) {
						{
							BlockPos _bp = new BlockPos(entity.getX(), targetHeight, entity.getZ());
							BlockState _bs = CrodecraftModBlocks.CREEP.get().defaultBlockState();
							BlockState _bso = world.getBlockState(_bp);
							for (Map.Entry<Property<?>, Comparable<?>> entry : _bso.getValues().entrySet()) {
								Property _property = _bs.getBlock().getStateDefinition().getProperty(entry.getKey().getName());
								if (_property != null && _bs.getValue(_property) != null)
									try {
										_bs = _bs.setValue(_property, (Comparable) entry.getValue());
									} catch (Exception e) {
									}
							}
							world.setBlock(_bp, _bs, 3);
						}
					}
				}
			}
		}
	}
}
