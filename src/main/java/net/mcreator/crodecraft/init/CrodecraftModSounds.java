
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.crodecraft.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.resources.ResourceLocation;

import java.util.Map;
import java.util.HashMap;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class CrodecraftModSounds {
	public static Map<ResourceLocation, SoundEvent> REGISTRY = new HashMap<>();
	static {
		REGISTRY.put(new ResourceLocation("crodecraft", "bosunbill"), new SoundEvent(new ResourceLocation("crodecraft", "bosunbill")));
		REGISTRY.put(new ResourceLocation("crodecraft", "zoom"), new SoundEvent(new ResourceLocation("crodecraft", "zoom")));
		REGISTRY.put(new ResourceLocation("crodecraft", "robama_hurt"), new SoundEvent(new ResourceLocation("crodecraft", "robama_hurt")));
		REGISTRY.put(new ResourceLocation("crodecraft", "fairybama_idle"), new SoundEvent(new ResourceLocation("crodecraft", "fairybama_idle")));
		REGISTRY.put(new ResourceLocation("crodecraft", "fairybama_hurt"), new SoundEvent(new ResourceLocation("crodecraft", "fairybama_hurt")));
		REGISTRY.put(new ResourceLocation("crodecraft", "robama_idle"), new SoundEvent(new ResourceLocation("crodecraft", "robama_idle")));
		REGISTRY.put(new ResourceLocation("crodecraft", "whatsupdoc"), new SoundEvent(new ResourceLocation("crodecraft", "whatsupdoc")));
		REGISTRY.put(new ResourceLocation("crodecraft", "michaelhurt.ogg"), new SoundEvent(new ResourceLocation("crodecraft", "michaelhurt.ogg")));
		REGISTRY.put(new ResourceLocation("crodecraft", "michaeldeath"), new SoundEvent(new ResourceLocation("crodecraft", "michaeldeath")));
		REGISTRY.put(new ResourceLocation("crodecraft", "michaellivibing"), new SoundEvent(new ResourceLocation("crodecraft", "michaellivibing")));
	}

	@SubscribeEvent
	public static void registerSounds(RegistryEvent.Register<SoundEvent> event) {
		for (Map.Entry<ResourceLocation, SoundEvent> sound : REGISTRY.entrySet())
			event.getRegistry().register(sound.getValue().setRegistryName(sound.getKey()));
	}
}
