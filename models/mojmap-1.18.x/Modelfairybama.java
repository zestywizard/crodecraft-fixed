// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.17 - 1.18 with Mojang mappings
// Paste this class into your mod and generate all required imports

public class Modelfairybama<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(
			new ResourceLocation("modid", "fairybama"), "main");
	private final ModelPart head;
	private final ModelPart rarm;
	private final ModelPart larm;
	private final ModelPart lleg;
	private final ModelPart rleg;
	private final ModelPart lwing;
	private final ModelPart rwing;
	private final ModelPart body;

	public Modelfairybama(ModelPart root) {
		this.head = root.getChild("head");
		this.rarm = root.getChild("rarm");
		this.larm = root.getChild("larm");
		this.lleg = root.getChild("lleg");
		this.rleg = root.getChild("rleg");
		this.lwing = root.getChild("lwing");
		this.rwing = root.getChild("rwing");
		this.body = root.getChild("body");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(
				-2.0F, -2.0F, -2.0F, 4.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 10.0F, 1.0F));

		PartDefinition rarm = partdefinition.addOrReplaceChild("rarm", CubeListBuilder.create().texOffs(14, 5).addBox(
				-1.0F, -2.5F, -1.5F, 2.0F, 5.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(3.0F, 14.5F, 1.5F));

		PartDefinition larm = partdefinition.addOrReplaceChild("larm", CubeListBuilder.create().texOffs(11, 14)
				.addBox(-6.0F, -8.2183F, -2.5977F, 2.0F, 5.0F, 3.0F, new CubeDeformation(0.0F)),
				PartPose.offset(2.0F, 20.2183F, 2.5977F));

		PartDefinition lleg = partdefinition.addOrReplaceChild("lleg", CubeListBuilder.create(),
				PartPose.offset(2.0F, 20.2183F, 2.5977F));

		PartDefinition cube_r1 = lleg
				.addOrReplaceChild("cube_r1",
						CubeListBuilder.create().texOffs(19, 20).addBox(-1.0F, -2.5F, -1.0F, 2.0F, 5.0F, 2.0F,
								new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition rleg = partdefinition.addOrReplaceChild("rleg", CubeListBuilder.create(),
				PartPose.offset(2.0F, 20.2183F, 2.5977F));

		PartDefinition cube_r2 = rleg
				.addOrReplaceChild("cube_r2",
						CubeListBuilder.create().texOffs(21, 13).addBox(-1.0F, -2.5F, -1.0F, 2.0F, 5.0F, 2.0F,
								new CubeDeformation(0.0F)),
						PartPose.offsetAndRotation(-4.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition lwing = partdefinition.addOrReplaceChild("lwing", CubeListBuilder.create(),
				PartPose.offset(0.0F, 15.0F, 3.5F));

		PartDefinition cube_r3 = lwing.addOrReplaceChild("cube_r3",
				CubeListBuilder.create().texOffs(0, 17).addBox(-1.6F, 1.0F, -1.2F, 4.0F, 6.0F, 1.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.247F, 0.1538F, 2.1064F));

		PartDefinition rwing = partdefinition.addOrReplaceChild("rwing", CubeListBuilder.create(),
				PartPose.offset(0.0F, 15.0F, 3.5F));

		PartDefinition cube_r4 = rwing.addOrReplaceChild("cube_r4",
				CubeListBuilder.create().texOffs(21, 0).addBox(-2.7F, 1.4F, -1.1F, 4.0F, 6.0F, 1.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.1299F, -0.2066F, -2.2342F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 8).addBox(
				-2.0F, -3.0F, -1.5F, 4.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 15.0F, 1.5F));

		return LayerDefinition.create(meshdefinition, 32, 32);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay,
			float red, float green, float blue, float alpha) {
		head.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		rarm.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		larm.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		lleg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		rleg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		lwing.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		rwing.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw,
			float headPitch) {
		this.head.yRot = netHeadYaw / (180F / (float) Math.PI);
		this.head.xRot = headPitch / (180F / (float) Math.PI);
		this.larm.xRot = Mth.cos(limbSwing * 0.6662F) * limbSwingAmount;
		this.rwing.xRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * limbSwingAmount;
		this.lwing.xRot = Mth.cos(limbSwing * 0.6662F) * limbSwingAmount;
		this.rarm.xRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * limbSwingAmount;
	}
}