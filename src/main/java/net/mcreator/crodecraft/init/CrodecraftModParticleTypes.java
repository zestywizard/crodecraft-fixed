
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.crodecraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.core.particles.ParticleType;

import net.mcreator.crodecraft.CrodecraftMod;

public class CrodecraftModParticleTypes {
	public static final DeferredRegister<ParticleType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.PARTICLE_TYPES, CrodecraftMod.MODID);
	public static final RegistryObject<ParticleType<?>> RADIATION = REGISTRY.register("radiation", () -> new SimpleParticleType(false));
}
