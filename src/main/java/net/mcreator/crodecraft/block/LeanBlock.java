
package net.mcreator.crodecraft.block;

import net.minecraft.world.level.material.MaterialColor;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.core.BlockPos;

import net.mcreator.crodecraft.init.CrodecraftModFluids;

public class LeanBlock extends LiquidBlock {
	public LeanBlock() {
		super(() -> (FlowingFluid) CrodecraftModFluids.LEAN.get(), BlockBehaviour.Properties.of(Material.WATER, MaterialColor.COLOR_PURPLE)
				.strength(100f).hasPostProcess((bs, br, bp) -> true).emissiveRendering((bs, br, bp) -> true)

		);
	}

	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter reader, BlockPos pos) {
		return true;
	}
}
