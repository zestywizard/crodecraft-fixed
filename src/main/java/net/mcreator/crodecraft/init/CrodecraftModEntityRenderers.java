
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.crodecraft.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.mcreator.crodecraft.client.renderer.RobamaRenderer;
import net.mcreator.crodecraft.client.renderer.PlankRenderer;
import net.mcreator.crodecraft.client.renderer.MichaelRenderer;
import net.mcreator.crodecraft.client.renderer.FairyBamaRenderer;
import net.mcreator.crodecraft.client.renderer.CreeplingRenderer;
import net.mcreator.crodecraft.client.renderer.BigChungusRenderer;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class CrodecraftModEntityRenderers {
	@SubscribeEvent
	public static void registerEntityRenderers(EntityRenderersEvent.RegisterRenderers event) {
		event.registerEntityRenderer(CrodecraftModEntities.CREEPLING.get(), CreeplingRenderer::new);
		event.registerEntityRenderer(CrodecraftModEntities.FAIRY_BAMA.get(), FairyBamaRenderer::new);
		event.registerEntityRenderer(CrodecraftModEntities.PLANK.get(), PlankRenderer::new);
		event.registerEntityRenderer(CrodecraftModEntities.BIG_CHUNGUS.get(), BigChungusRenderer::new);
		event.registerEntityRenderer(CrodecraftModEntities.ROBAMA.get(), RobamaRenderer::new);
		event.registerEntityRenderer(CrodecraftModEntities.MICHAEL.get(), MichaelRenderer::new);
	}
}
