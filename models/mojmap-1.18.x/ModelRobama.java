// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.17 - 1.18 with Mojang mappings
// Paste this class into your mod and generate all required imports

public class ModelRobama<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(
			new ResourceLocation("modid", "robama"), "main");
	private final ModelPart larm;
	private final ModelPart rarm;
	private final ModelPart rleg;
	private final ModelPart head;
	private final ModelPart lleg;
	private final ModelPart body;

	public ModelRobama(ModelPart root) {
		this.larm = root.getChild("larm");
		this.rarm = root.getChild("rarm");
		this.rleg = root.getChild("rleg");
		this.head = root.getChild("head");
		this.lleg = root.getChild("lleg");
		this.body = root.getChild("body");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition larm = partdefinition.addOrReplaceChild("larm", CubeListBuilder.create(),
				PartPose.offsetAndRotation(4.7783F, 0.8472F, 2.5F, 0.2618F, 0.0F, 0.4363F));

		PartDefinition cube_r1 = larm.addOrReplaceChild("cube_r1",
				CubeListBuilder.create().texOffs(0, 60).addBox(-9.2278F, 6.0893F, 7.9471F, 5.0F, 14.0F, 5.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-3.8399F, 5.2421F, -3.9928F, -1.4105F, 1.2188F, -2.1004F));

		PartDefinition cube_r2 = larm.addOrReplaceChild("cube_r2",
				CubeListBuilder.create().texOffs(26, 52).addBox(-7.198F, -1.54F, 6.9471F, 6.0F, 11.0F, 7.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-3.8399F, 5.2421F, -3.9928F, 1.3228F, 1.3447F, 0.6487F));

		PartDefinition rarm = partdefinition.addOrReplaceChild("rarm", CubeListBuilder.create(),
				PartPose.offsetAndRotation(-5.2217F, 0.8472F, 2.5F, -0.2618F, 0.0F, 0.4363F));

		PartDefinition cube_r3 = rarm.addOrReplaceChild("cube_r3",
				CubeListBuilder.create().texOffs(52, 52).addBox(-8.9497F, 5.6528F, -11.0153F, 5.0F, 14.0F, 5.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(5.2232F, 2.454F, -1.9306F, 1.116F, 1.4452F, 0.8997F));

		PartDefinition cube_r4 = rarm.addOrReplaceChild("cube_r4",
				CubeListBuilder.create().texOffs(0, 42).addBox(-7.198F, -2.0577F, -12.0153F, 6.0F, 11.0F, 7.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(5.2232F, 2.454F, -1.9306F, 1.4834F, 0.8888F, 1.2835F));

		PartDefinition rleg = partdefinition.addOrReplaceChild("rleg", CubeListBuilder.create(),
				PartPose.offset(-4.0F, 14.0F, -4.5F));

		PartDefinition cube_r5 = rleg.addOrReplaceChild("cube_r5",
				CubeListBuilder.create().texOffs(31, 35).addBox(-3.7216F, 9.25F, -8.0F, 12.0F, 10.0F, 7.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(3.7216F, -9.25F, 6.5F, 0.0F, 1.5708F, 0.0F));

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create(),
				PartPose.offset(-0.2784F, 1.75F, 0.9716F));

		PartDefinition cube_r6 = head.addOrReplaceChild("cube_r6",
				CubeListBuilder.create().texOffs(0, 0)
						.addBox(0.2784F, -16.75F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(42, 17)
						.addBox(-2.7216F, -11.75F, -4.0F, 8.0F, 8.0F, 8.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 3.0F, 1.0284F, 0.0F, 1.5708F, 0.0F));

		PartDefinition lleg = partdefinition.addOrReplaceChild("lleg", CubeListBuilder.create(),
				PartPose.offset(4.2216F, 15.0F, -0.2784F));

		PartDefinition cube_r7 = lleg.addOrReplaceChild("cube_r7",
				CubeListBuilder.create().texOffs(0, 25).addBox(-3.7216F, 9.25F, 1.0F, 12.0F, 10.0F, 7.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-4.5F, -10.25F, 2.2784F, 0.0F, 1.5708F, 0.0F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create(),
				PartPose.offset(-0.5F, 7.5F, 0.0F));

		PartDefinition cube_r8 = body.addOrReplaceChild("cube_r8",
				CubeListBuilder.create().texOffs(0, 0).addBox(-5.7216F, -3.75F, -6.0F, 13.0F, 13.0F, 12.0F,
						new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.2216F, -2.75F, 2.0F, 0.0F, 1.5708F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay,
			float red, float green, float blue, float alpha) {
		larm.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		rarm.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		rleg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		head.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		lleg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw,
			float headPitch) {
		this.head.yRot = netHeadYaw / (180F / (float) Math.PI);
		this.head.xRot = headPitch / (180F / (float) Math.PI);
		this.lleg.xRot = Mth.cos(limbSwing * 1.0F) * -1.0F * limbSwingAmount;
		this.larm.xRot = Mth.cos(limbSwing * 0.6662F) * limbSwingAmount;
		this.rleg.xRot = Mth.cos(limbSwing * 1.0F) * 1.0F * limbSwingAmount;
		this.rarm.xRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * limbSwingAmount;
	}
}