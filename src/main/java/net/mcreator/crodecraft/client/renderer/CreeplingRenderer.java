
package net.mcreator.crodecraft.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.model.geom.ModelLayers;
import net.minecraft.client.model.SilverfishModel;

import net.mcreator.crodecraft.entity.CreeplingEntity;

public class CreeplingRenderer extends MobRenderer<CreeplingEntity, SilverfishModel<CreeplingEntity>> {
	public CreeplingRenderer(EntityRendererProvider.Context context) {
		super(context, new SilverfishModel(context.bakeLayer(ModelLayers.SILVERFISH)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(CreeplingEntity entity) {
		return new ResourceLocation("crodecraft:textures/creepling.png");
	}
}
