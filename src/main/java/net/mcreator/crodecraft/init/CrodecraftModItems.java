
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.crodecraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.common.ForgeSpawnEggItem;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.crodecraft.item.TheHiveItem;
import net.mcreator.crodecraft.item.LeanItem;
import net.mcreator.crodecraft.item.HurdyGurdyItem;
import net.mcreator.crodecraft.item.FairybamaForestsItem;
import net.mcreator.crodecraft.item.EpicFaceItem;
import net.mcreator.crodecraft.item.EpicArmorItem;
import net.mcreator.crodecraft.item.CocaineItem;
import net.mcreator.crodecraft.item.CocaBranchItem;
import net.mcreator.crodecraft.item.BarryRealmItem;
import net.mcreator.crodecraft.CrodecraftMod;

public class CrodecraftModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, CrodecraftMod.MODID);
	public static final RegistryObject<Item> EPIC_ORE = block(CrodecraftModBlocks.EPIC_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> EPIC_FACE = REGISTRY.register("epic_face", () -> new EpicFaceItem());
	public static final RegistryObject<Item> THE_HIVE = REGISTRY.register("the_hive", () -> new TheHiveItem());
	public static final RegistryObject<Item> DENIM = block(CrodecraftModBlocks.DENIM, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> OBAMA = block(CrodecraftModBlocks.OBAMA, CreativeModeTab.TAB_DECORATIONS);
	public static final RegistryObject<Item> EPIC_ARMOR_HELMET = REGISTRY.register("epic_armor_helmet", () -> new EpicArmorItem.Helmet());
	public static final RegistryObject<Item> CREEP = block(CrodecraftModBlocks.CREEP, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> CREEPLING = REGISTRY.register("creepling_spawn_egg",
			() -> new ForgeSpawnEggItem(CrodecraftModEntities.CREEPLING, -1, -1, new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
	public static final RegistryObject<Item> LEAN_BUCKET = REGISTRY.register("lean_bucket", () -> new LeanItem());
	public static final RegistryObject<Item> COCA = block(CrodecraftModBlocks.COCA, CreativeModeTab.TAB_DECORATIONS);
	public static final RegistryObject<Item> COCA_BRANCH = REGISTRY.register("coca_branch", () -> new CocaBranchItem());
	public static final RegistryObject<Item> HURDY_GURDY = REGISTRY.register("hurdy_gurdy", () -> new HurdyGurdyItem());
	public static final RegistryObject<Item> COCA_PASTE = block(CrodecraftModBlocks.COCA_PASTE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> COCAINE = REGISTRY.register("cocaine", () -> new CocaineItem());
	public static final RegistryObject<Item> COCA_BLOCK = block(CrodecraftModBlocks.COCA_BLOCK, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> FAIRY_BAMA = REGISTRY.register("fairy_bama_spawn_egg",
			() -> new ForgeSpawnEggItem(CrodecraftModEntities.FAIRY_BAMA, -1, -26113, new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
	public static final RegistryObject<Item> PLANK = REGISTRY.register("plank_spawn_egg",
			() -> new ForgeSpawnEggItem(CrodecraftModEntities.PLANK, -3750260, -1, new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
	public static final RegistryObject<Item> BIG_CHUNGUS = REGISTRY.register("big_chungus_spawn_egg",
			() -> new ForgeSpawnEggItem(CrodecraftModEntities.BIG_CHUNGUS, -1, -1, new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
	public static final RegistryObject<Item> ROBAMA = REGISTRY.register("robama_spawn_egg",
			() -> new ForgeSpawnEggItem(CrodecraftModEntities.ROBAMA, -10066330, -16764007, new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
	public static final RegistryObject<Item> BA_ROCK = block(CrodecraftModBlocks.BA_ROCK, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> FAIRYGRASS = block(CrodecraftModBlocks.FAIRYGRASS, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> BARRY_REALM = REGISTRY.register("barry_realm", () -> new BarryRealmItem());
	public static final RegistryObject<Item> MICHAEL = REGISTRY.register("michael_spawn_egg",
			() -> new ForgeSpawnEggItem(CrodecraftModEntities.MICHAEL, -15263977, -3336425, new Item.Properties().tab(CreativeModeTab.TAB_MISC)));
	public static final RegistryObject<Item> BEANSTALK = block(CrodecraftModBlocks.BEANSTALK, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final RegistryObject<Item> FAIRYBAMA_FORESTS = REGISTRY.register("fairybama_forests", () -> new FairybamaForestsItem());
	public static final RegistryObject<Item> FROOM = block(CrodecraftModBlocks.FROOM, CreativeModeTab.TAB_DECORATIONS);
	public static final RegistryObject<Item> OBAMEGG = block(CrodecraftModBlocks.OBAMEGG, CreativeModeTab.TAB_BUILDING_BLOCKS);

	private static RegistryObject<Item> block(RegistryObject<Block> block, CreativeModeTab tab) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
	}
}
