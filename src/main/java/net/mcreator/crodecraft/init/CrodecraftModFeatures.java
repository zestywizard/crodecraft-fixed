
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.crodecraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;

import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.core.Holder;

import net.mcreator.crodecraft.world.features.plants.ObamaFeature;
import net.mcreator.crodecraft.world.features.plants.FroomFeature;
import net.mcreator.crodecraft.world.features.plants.CocaFeature;
import net.mcreator.crodecraft.world.features.ores.ObameggFeature;
import net.mcreator.crodecraft.world.features.ores.EpicOreFeature;
import net.mcreator.crodecraft.world.features.lakes.LeanFeature;
import net.mcreator.crodecraft.CrodecraftMod;

import java.util.function.Supplier;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber
public class CrodecraftModFeatures {
	public static final DeferredRegister<Feature<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.FEATURES, CrodecraftMod.MODID);
	private static final List<FeatureRegistration> FEATURE_REGISTRATIONS = new ArrayList<>();
	public static final RegistryObject<Feature<?>> EPIC_ORE = register("epic_ore", EpicOreFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, EpicOreFeature.GENERATE_BIOMES, EpicOreFeature::placedFeature));
	public static final RegistryObject<Feature<?>> OBAMA = register("obama", ObamaFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.VEGETAL_DECORATION, ObamaFeature.GENERATE_BIOMES, ObamaFeature::placedFeature));
	public static final RegistryObject<Feature<?>> LEAN = register("lean", LeanFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.LAKES, LeanFeature.GENERATE_BIOMES, LeanFeature::placedFeature));
	public static final RegistryObject<Feature<?>> COCA = register("coca", CocaFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.VEGETAL_DECORATION, CocaFeature.GENERATE_BIOMES, CocaFeature::placedFeature));
	public static final RegistryObject<Feature<?>> FROOM = register("froom", FroomFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.VEGETAL_DECORATION, FroomFeature.GENERATE_BIOMES, FroomFeature::placedFeature));
	public static final RegistryObject<Feature<?>> OBAMEGG = register("obamegg", ObameggFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, ObameggFeature.GENERATE_BIOMES, ObameggFeature::placedFeature));

	private static RegistryObject<Feature<?>> register(String registryname, Supplier<Feature<?>> feature, FeatureRegistration featureRegistration) {
		FEATURE_REGISTRATIONS.add(featureRegistration);
		return REGISTRY.register(registryname, feature);
	}

	@SubscribeEvent
	public static void addFeaturesToBiomes(BiomeLoadingEvent event) {
		for (FeatureRegistration registration : FEATURE_REGISTRATIONS) {
			if (registration.biomes() == null || registration.biomes().contains(event.getName()))
				event.getGeneration().getFeatures(registration.stage()).add(registration.placedFeature().get());
		}
	}

	private static record FeatureRegistration(GenerationStep.Decoration stage, Set<ResourceLocation> biomes,
			Supplier<Holder<PlacedFeature>> placedFeature) {
	}
}
