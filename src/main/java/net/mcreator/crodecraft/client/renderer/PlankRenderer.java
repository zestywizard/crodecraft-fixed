
package net.mcreator.crodecraft.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.crodecraft.entity.PlankEntity;
import net.mcreator.crodecraft.client.model.ModelPlank;

public class PlankRenderer extends MobRenderer<PlankEntity, ModelPlank<PlankEntity>> {
	public PlankRenderer(EntityRendererProvider.Context context) {
		super(context, new ModelPlank(context.bakeLayer(ModelPlank.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(PlankEntity entity) {
		return new ResourceLocation("crodecraft:textures/plank_2.png");
	}
}
