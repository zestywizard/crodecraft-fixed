
/*
 * MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.crodecraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.material.Fluid;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.ItemBlockRenderTypes;

import net.mcreator.crodecraft.fluid.LeanFluid;
import net.mcreator.crodecraft.CrodecraftMod;

public class CrodecraftModFluids {
	public static final DeferredRegister<Fluid> REGISTRY = DeferredRegister.create(ForgeRegistries.FLUIDS, CrodecraftMod.MODID);
	public static final RegistryObject<Fluid> LEAN = REGISTRY.register("lean", () -> new LeanFluid.Source());
	public static final RegistryObject<Fluid> FLOWING_LEAN = REGISTRY.register("flowing_lean", () -> new LeanFluid.Flowing());

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientSideHandler {
		@SubscribeEvent
		public static void clientSetup(FMLClientSetupEvent event) {
			ItemBlockRenderTypes.setRenderLayer(LEAN.get(), renderType -> renderType == RenderType.translucent());
			ItemBlockRenderTypes.setRenderLayer(FLOWING_LEAN.get(), renderType -> renderType == RenderType.translucent());
		}
	}
}
